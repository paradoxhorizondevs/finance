package com.paradoxhorizon.finance.infrastructure.supabase;

import com.paradoxhorizon.finance.config.security.provider.UsernamePasswordAuthTokenProvider;
import com.paradoxhorizon.finance.config.security.service.impl.UserServiceImpl;
import com.paradoxhorizon.finance.domain.UserDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class SupabaseAuthApiImplTest {

    @Mock
    private RestTemplate restTemplate;
    @Mock
    private UsernamePasswordAuthTokenProvider tokenProvider;
    @Mock
    private UserServiceImpl userService;
    private SupabaseAuthApiImpl supabaseAuthApi;

    @BeforeEach
    void init() {
        initMocks(this);
        this.supabaseAuthApi = new SupabaseAuthApiImpl(restTemplate, tokenProvider, userService);
    }

    @Test
    void testSignup() {
        String uuid = "5673236e-02dc-4284-aa2b-c90d5078a60b";
        String email = "test@email.com";
        String phone = null;
        String aud = "authenticated";
        String role = "";

        String password = "password";

        String accessToken = "eyJhbGciOiJIUzI1NiIsImtpZCI6IkhENlNmSWVaZGl2M2J0cXciLCJ0eXAiOiJKV1QifQ.eyJhdWQiOiJhdXRoZW50aWNhdGVkIiwiZXhwIjoxNjk4NjY2MzkyLCJpYXQiOjE2OTg2NjI3OTIsImlzcyI6Imh0dHBzOi8vanhqaWJkcGZ6cGZkcnlqa3VjZXEuc3VwYWJhc2UuY28vYXV0aC92MSIsInN1YiI6IjU2NzMyMzZlLTAyZGMtNDI4NC1hYTJiLWM5MGQ1MDc4YTYwYiIsImVtYWlsIjoidGVzdEBlbWFpbC5jb20iLCJwaG9uZSI6IiIsImFwcF9tZXRhZGF0YSI6eyJwcm92aWRlciI6ImVtYWlsIiwicHJvdmlkZXJzIjpbImVtYWlsIl19LCJ1c2VyX21ldGFkYXRhIjp7fSwicm9sZSI6ImF1dGhlbnRpY2F0ZWQiLCJhYWwiOiJhYWwxIiwiYW1yIjpbeyJtZXRob2QiOiJwYXNzd29yZCIsInRpbWVzdGFtcCI6MTY5ODY2Mjc5Mn1dLCJzZXNzaW9uX2lkIjoiNzI3ZDA4ZDEtYzJmZC00ODEwLTkxMDUtMzU2YjRmNDA1YzUwIn0.zxnCwB57b22uhBk84D4sUsf-HnEfWzPopqQ74S3mLiI";
        String tokenType = "bearer";
        long expiresIn = 0;
        long expiresAt = 0;
        String refreshToken = "wgHa6UKmcrflkapxvoHskg";

        SupabaseUserResponse user = SupabaseUserResponse.builder()
                .id(uuid)
                .aud(aud)
                .role(role)
                .email(email)
                .phone(phone)
                .build();

        SupabaseAuthResponse response = SupabaseAuthResponse.builder()
                .accessToken(accessToken)
                .tokenType(tokenType)
                .expiresIn(expiresIn)
                .expiresAt(expiresAt)
                .refreshToken(refreshToken)
                .user(user)
                .build();

        when(restTemplate.exchange(
                anyString(),
                any(HttpMethod.class),
                any(HttpEntity.class),
                eq(SupabaseAuthResponse.class)
        )).thenReturn(ResponseEntity.ok(response));

        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_AUTHENTICATED"));

        when(userService.loadUserByUsername(user.getId()))
                .thenReturn(new User(user.getId(), "", authorities));

        UserDto userDto = UserDto.builder()
                .email(email)
                .password(password)
                .build();

        SupabaseAuthResponse testResponse = supabaseAuthApi.signup(userDto);

        assertEquals(testResponse, response);
    }
}
