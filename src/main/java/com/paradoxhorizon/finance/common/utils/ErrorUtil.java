package com.paradoxhorizon.finance.common.utils;

import com.paradoxhorizon.finance.config.errorhandling.RestDetailedMessage;
import com.paradoxhorizon.finance.config.errorhandling.RestErrorResponse;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;

import java.sql.Timestamp;

@Service
public class ErrorUtil {

    // TODO: The below logic works but it is hard coded to decode Supabase error responses.
    //  It works for now because the app is only using Supabase but a more robust logic must
    //  be created in the future to support other third party APIs.
    public RestErrorResponse buildErrorMessage(HttpStatusCodeException e) {
        String errorMessage = e.getMessage();
        String sub = errorMessage.substring(errorMessage.indexOf("{"), errorMessage.indexOf("}") + 1);
        JSONObject keyValuePair = new JSONObject(sub);
        RestDetailedMessage detailedMessage = new RestDetailedMessage();
        detailedMessage.setCode(keyValuePair.getInt("code"));
        detailedMessage.setMsg(keyValuePair.getString("msg"));

        return RestErrorResponse.builder()
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .httpStatus((HttpStatus) e.getStatusCode())
                .status(e.getStatusCode().value())
                .error(e.getStatusText())
                .message(detailedMessage)
                .build();
    }
}
