package com.paradoxhorizon.finance.web.authentication;

import com.paradoxhorizon.finance.domain.UserDto;

public interface AuthenticationService {
    UserDto getLoginCredentials(AuthenticationRequest authenticationRequest);
}
