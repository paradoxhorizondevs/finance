package com.paradoxhorizon.finance.web.authentication;

import com.paradoxhorizon.finance.domain.UserDto;
import com.paradoxhorizon.finance.infrastructure.supabase.SupabaseAuthApi;
import com.paradoxhorizon.finance.infrastructure.supabase.SupabaseAuthResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/auth")
@RestController
public class AuthenticationController {

    private final SupabaseAuthApi supabaseAuthApi;
    private final AuthenticationService authenticationService;

    public AuthenticationController(SupabaseAuthApi supabaseAuthApi, AuthenticationServiceImpl authenticationService) {
        this.supabaseAuthApi = supabaseAuthApi;
        this.authenticationService = authenticationService;
    }

    @PostMapping("/login")
    public ResponseEntity<SupabaseAuthResponse> login(@RequestBody AuthenticationRequest authRequest) {
        UserDto userDto = authenticationService.getLoginCredentials(authRequest);
        return new ResponseEntity<>(supabaseAuthApi.login(userDto), HttpStatus.OK);
    }

    @PostMapping("/signup")
    public ResponseEntity<SupabaseAuthResponse> signup(@RequestBody AuthenticationRequest authRequest) {
        UserDto userDto = authenticationService.getLoginCredentials(authRequest);
        return new ResponseEntity<>(supabaseAuthApi.signup(userDto), HttpStatus.OK);
    }
}
