package com.paradoxhorizon.finance.web.authentication;

import com.paradoxhorizon.finance.domain.UserDto;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    @Override
    public UserDto getLoginCredentials(AuthenticationRequest authenticationRequest) {
        UserDto userDto = new UserDto();
        userDto.setEmail(authenticationRequest.getEmail());
        userDto.setPassword(authenticationRequest.getPassword());
        return userDto;
    }
}
