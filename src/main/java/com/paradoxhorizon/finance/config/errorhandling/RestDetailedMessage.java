package com.paradoxhorizon.finance.config.errorhandling;

import lombok.Data;

@Data
public class RestDetailedMessage {

    private int code;
    private String msg;
}
