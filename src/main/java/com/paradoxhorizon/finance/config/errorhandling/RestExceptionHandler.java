package com.paradoxhorizon.finance.config.errorhandling;

import com.paradoxhorizon.finance.common.utils.ErrorUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @Autowired
    private ErrorUtil errorUtil;

    @ExceptionHandler(HttpStatusCodeException.class)
    protected ResponseEntity<Object> handleAuthenticationError(
            HttpStatusCodeException ex) {
        return buildResponseEntity(errorUtil.buildErrorMessage(ex));
    }

    private ResponseEntity<Object> buildResponseEntity(RestErrorResponse errorResponse) {
        return (errorResponse.getHttpStatus() != null)
                ? new ResponseEntity<>(errorResponse, errorResponse.getHttpStatus())
                : new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
