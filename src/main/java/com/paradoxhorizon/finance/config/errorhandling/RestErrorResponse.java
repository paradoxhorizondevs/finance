package com.paradoxhorizon.finance.config.errorhandling;

import com.fasterxml.jackson.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.http.HttpStatus;


import java.sql.Timestamp;

@Data
@Builder
@AllArgsConstructor
public class RestErrorResponse {

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private Timestamp timestamp;
    @JsonIgnore
    private HttpStatus httpStatus;
    private int status;
    private String error;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String path;
    private RestDetailedMessage message;
}
