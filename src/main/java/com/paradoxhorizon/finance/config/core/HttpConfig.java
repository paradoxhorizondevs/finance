package com.paradoxhorizon.finance.config.core;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class HttpConfig {

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {

            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedOrigins("*")
                        .allowedHeaders(
                                "AUTHORIZATION",
                                "CACHE_CONTROL",
                                "CONTENT_TYPE"
                        )
                        .allowedMethods(
                                "HEAD",
                                "GET",
                                "POST",
                                "PUT",
                                "DELETE",
                                "PATCH"
                        )
                        .exposedHeaders("CONTENT_DISPOSITION")
                        .maxAge(3600);
            }
        };
    }
}
