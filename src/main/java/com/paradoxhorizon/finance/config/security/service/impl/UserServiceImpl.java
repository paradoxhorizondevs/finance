package com.paradoxhorizon.finance.config.security.service.impl;

import com.paradoxhorizon.finance.infrastructure.supabase.SupabaseProfileApiImpl;
import com.paradoxhorizon.finance.infrastructure.supabase.SupabaseUserResponse;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
public class UserServiceImpl implements UserDetailsService {

    private final SupabaseProfileApiImpl supabaseProfileApiImpl;

    public UserServiceImpl(SupabaseProfileApiImpl supabaseProfileApiImpl) {
        this.supabaseProfileApiImpl = supabaseProfileApiImpl;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        SupabaseUserResponse userResponse = supabaseProfileApiImpl.getUserWithId(username);

        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_AUTHENTICATED"));
        return new User(userResponse.getId(), "", authorities);
    }
}
