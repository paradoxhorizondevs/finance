package com.paradoxhorizon.finance.config.security.filter;

import com.paradoxhorizon.finance.common.utils.JwtUtil;
import com.paradoxhorizon.finance.config.security.provider.UsernamePasswordAuthTokenProvider;
import com.paradoxhorizon.finance.config.security.service.impl.UserServiceImpl;
import com.paradoxhorizon.finance.common.structure.HttpStructure;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    @Autowired
    private JwtUtil jwtUtil;
    @Autowired
    private UsernamePasswordAuthTokenProvider tokenProvider;
    @Autowired
    private UserServiceImpl userService;

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {
        logger.info("Entering JwtAuthenticationFilter.");

        String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
        String userId = null;
        String authToken;

        if (authHeader != null && authHeader.startsWith(HttpStructure.Header.BEARER_PREFIX)) {
            authToken = authHeader.replace(HttpStructure.Header.BEARER_PREFIX, "");

            // TODO: These exception should apply only to routes that need to be authenticated.
            //  Find a way to prevent exceptions from preventing access to public routes.
            try {
                userId = jwtUtil.getUsernameFromToken(authToken);
            } catch (IllegalArgumentException e) {
                logger.warn("An error occurred while fetching username from token", e);
                SecurityContextHolder.clearContext();
            } catch (MalformedJwtException e) {
                logger.warn("The token is invalid.", e);
                SecurityContextHolder.clearContext();
            } catch (ExpiredJwtException e) {
                logger.warn("The token has expired.", e);
                SecurityContextHolder.clearContext();
            } catch(SignatureException e){
                logger.warn("Authentication failed. Token signature not valid.");
                SecurityContextHolder.clearContext();
            }
        } else {
            SecurityContextHolder.clearContext();
            logger.warn("Couldn't find bearer string, header will be ignored.");
        }

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (userId != null && authentication == null) {
            UserDetails userDetails = userService.loadUserByUsername(userId);
            UsernamePasswordAuthenticationToken authenticationToken = tokenProvider.createToken(userDetails);
            SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        }

        filterChain.doFilter(request, response);
    }
}
