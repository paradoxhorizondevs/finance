package com.paradoxhorizon.finance.infrastructure.supabase;

import com.paradoxhorizon.finance.domain.UserDto;

public interface SupabaseAuthApi {

    SupabaseAuthResponse signup(UserDto userDto);
    SupabaseAuthResponse login(UserDto userDto);
}
