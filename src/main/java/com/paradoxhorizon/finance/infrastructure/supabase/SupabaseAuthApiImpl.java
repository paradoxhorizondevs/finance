package com.paradoxhorizon.finance.infrastructure.supabase;

import com.paradoxhorizon.finance.config.security.provider.UsernamePasswordAuthTokenProvider;
import com.paradoxhorizon.finance.config.security.service.impl.UserServiceImpl;
import com.paradoxhorizon.finance.common.structure.HttpStructure;
import com.paradoxhorizon.finance.domain.UserDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Objects;

@Service
public class SupabaseAuthApiImpl implements SupabaseAuthApi {

    @Value("${supabase.key.public}")
    private String supabasePublicKey;
    @Value("${supabase.server}")
    private String supabaseServer;
    private final UsernamePasswordAuthTokenProvider tokenProvider;
    private final UserServiceImpl userService;
    private final RestTemplate restTemplate;

    public SupabaseAuthApiImpl(RestTemplate restTemplate,
                               UsernamePasswordAuthTokenProvider tokenProvider,
                               UserServiceImpl userService) {
        this.restTemplate = restTemplate;
        this.userService = userService;
        this.tokenProvider = tokenProvider;
    }

    public SupabaseAuthResponse signup(UserDto userDto) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add(SupabaseStructure.Header.API_KEY, supabasePublicKey);

        String resourceUrl = supabaseServer + SupabaseEndpoint.Authentication.SIGNUP;

        HttpEntity<UserDto> entity = new HttpEntity<>(userDto, headers);
        ResponseEntity<SupabaseAuthResponse> response = restTemplate.exchange(
                resourceUrl,
                HttpMethod.POST,
                entity,
                SupabaseAuthResponse.class);

        SupabaseUserResponse userResponse = Objects.requireNonNull(response.getBody()).getUser();
        updateSecurityContext(userResponse.getId());
        return response.getBody();
    }

    public SupabaseAuthResponse login(UserDto userDto) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add(SupabaseStructure.Header.API_KEY, supabasePublicKey);

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        String queryValue = HttpStructure.Parameter.PASSWORD;
        params.add(HttpStructure.Parameter.GRANT_TYPE, queryValue);

        String resourceUrl = supabaseServer + SupabaseEndpoint.Authentication.TOKEN;
        URI url = UriComponentsBuilder
                .fromUriString(resourceUrl)
                .queryParams(params)
                .build()
                .toUri();

        HttpEntity<UserDto> entity = new HttpEntity<>(userDto, headers);
        ResponseEntity<SupabaseAuthResponse> response = restTemplate.exchange(
                url,
                HttpMethod.POST,
                entity,
                SupabaseAuthResponse.class
        );

        SupabaseUserResponse userResponse = Objects.requireNonNull(response.getBody()).getUser();
        updateSecurityContext(userResponse.getId());
        return response.getBody();
    }

    private void updateSecurityContext(String userId) {
        UserDetails userDetails = userService.loadUserByUsername(userId);
        UsernamePasswordAuthenticationToken authenticationToken = tokenProvider.createToken(userDetails);
        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
    }
}
