package com.paradoxhorizon.finance.infrastructure.supabase;

public interface SupabaseProfileApi {

    SupabaseUserResponse getUserWithId(String id);
}
